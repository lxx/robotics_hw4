Homework 4, Computational Aspects of Robotics
=============================================

> Team 16: Ruilin Xu, Qi Wang, Haoxiang Xu, He Li

### Part 1(Path Planning)

We implement the path planning part in `python 2.7`, with [`Tkinter`](https://wiki.python.org/moin/TkInter).
`Tkinter` module is the standard Python interface to the Tk GUI toolkit and **it is already included in almost
all recent python releases**.

#### How to install Tkinter if I don't have it?

If you are using python 2.7 provided by the system on Ubuntu, type the following command to install Tkinter.
```
$ sudo apt-get install python-tk
```

For Mac OS, download [python 2.7.10](https://www.python.org/ftp/python/2.7.10/python-2.7.10-macosx10.6.pkg) and
install it. You have your `Tkinter` module installed too.

For Windows, download [python 2.7.10](https://www.python.org/ftp/python/2.7.10/python-2.7.10.msi) and install it.
You have your `Tkinter` module installed too.


#### How to run the program?

With only one command line:
```
$ python path_planning.py
```

The program will graphically show the process of each step:
* draw the start, goal point and all the obstacles
* compute the convex hulls and draw them on the canvas
* validate all edges and draw the valid ones on the canvas
* compute the shortest path, and paint it with red color to make it noticeable
* generate the file named `transit` to cache the path, so matlab program can read the planned path from it

#### How we implement the algorithm?

##### Reflection algorithm

At line 120, the function name is `grow_obstacles`.

##### Convex hull algorithm

At line 113, the function name is `convex_hull`.

##### Validate all the edges

At line 188, the function name is `generate_path`.

##### Dijkstra's algorithm (Find the shortest path)

At line 218, the function name is `generate_minimum_path`

#### What's more

The screenshot of the graphical output of our path planner is in the image file named `path_planning.png`
The sample output path planning file is the file named `transit`