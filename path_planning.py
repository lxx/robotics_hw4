"""
Route Planning
team 16

"""
import threading
from Tkinter import *
from time import sleep
import math

__author__ = 'He Li, Haoxiang Xu'
__version__ = '10.0'

ERR_TOLERANCE = 0.000001
MAX_DISTANCE = 100000.0
SLEEP_TIME = 1.5
TURN_LEFT, TURN_RIGHT, TURN_NONE = (1, -1, 0)

# GOAL_FILE_PATH = "./assets/test_start_goal.txt"
# OBSTACLE_FILE_PATH = "./assets/test_obstacles.txt"
GOAL_FILE_PATH = "./assets/start_goal.txt"
OBSTACLE_FILE_PATH = "./assets/obstacles.txt"
TRANSIT_RESULT_FILE_PATH = "transit"

DIAMETER = 0.40
ROBOT_CONTOUR = [[DIAMETER / 2, DIAMETER / 2], [-DIAMETER / 2, -DIAMETER / 2],
                 [-DIAMETER / 2, DIAMETER / 2], [DIAMETER / 2, -DIAMETER / 2]]
WORLD_CONTOUR = []
START_POINT = [0.0, 0.0]
GOAL_POINT = [0.0, 0.0]


def equal_v_v(v1, v2):
    return v1[0] == v2[0] and v1[1] == v2[1]


def add_v_v(v1, v2):
    return [v1[0] + v2[0], v1[1] + v2[1]]


def sub_v_v(v1, v2):
    return [v1[0] - v2[0], v1[1] - v2[1]]


def multi_v_n(v, n):
    return [v[0] * n, v[1] * n]


def cross_v_v(v1, v2):
    return v1[0] * v2[1] - v1[1] * v2[0]


def dot_v_v(v1, v2):
    return v1[0] * v2[0] + v1[1] * v2[1]


def normalize(v):
    n = math.sqrt(dot_v_v(v, v))
    if n < ERR_TOLERANCE:
        return [0, 0]
    else:
        return multi_v_n(v, 1 / n)


def distance(p1, p2):
    return math.sqrt(math.pow(p1[0] - p2[0], 2) + math.pow(p1[1] - p2[1], 2))


def read_obstacles(goal_file_path, obstacle_file_path):
    content = [record.rstrip('\n') for record in open(goal_file_path)]
    start_point_str = str.split(content[0])
    end_point_str = str.split(content[1])
    START_POINT[0], START_POINT[1] = float(start_point_str[0]), float(start_point_str[1])
    GOAL_POINT[0], GOAL_POINT[1] = float(end_point_str[0]), float(end_point_str[1])

    content = [record.rstrip('\n') for record in open(obstacle_file_path)]
    line_counter = 0
    obstacle_set = []
    obstacle_num = int(content[line_counter])
    line_counter += 1

    for obstacle_index in range(0, obstacle_num):
        obstacle = []
        points_num = int(content[line_counter])
        line_counter += 1

        for point_index in range(0, points_num):
            point = []
            point_pair = str.split(content[line_counter])
            point.append(float(point_pair[0]))
            point.append(float(point_pair[1]))
            obstacle.append(point)
            line_counter += 1
        if obstacle_index == 0:
            obstacle.reverse()
            WORLD_CONTOUR[:] = obstacle[:]
        obstacle_set.append(obstacle)
    return obstacle_set


def turn(p, q, r):
    return cmp((q[0] - p[0]) * (r[1] - p[1]) - (r[0] - p[0]) * (q[1] - p[1]), 0)


def _keep_left_(hull, r):
    while len(hull) > 1 and turn(hull[-2], hull[-1], r) != TURN_LEFT:
        hull.pop()
    if not len(hull) or hull[-1] != r:
        hull.append(r)
    return hull


def convex_hull(points):
    points = sorted(points)
    l = reduce(_keep_left_, points, [])
    u = reduce(_keep_left_, reversed(points), [])
    return l.extend(u[i] for i in xrange(1, len(u) - 1)) or l


def grow_obstacles(obstacle_set, contour):
    for position in contour:
        position[0], position[1] = -1 * position[0], -1 * position[1]
    grown_obstacle_set = []

    for index, obstacle in enumerate(obstacle_set):
        if index != 0:
            grown_points = []
            for point in obstacle:
                grown_points.append(point)
                for position in contour:
                    grown_points.append([point[0] + position[0], point[1] + position[1]])
            grown_obstacle_set.append(convex_hull(grown_points))
    return grown_obstacle_set


def side_check(p1, p2, cx, cy):
    return cross_v_v(normalize(sub_v_v(p1, cx)), normalize(sub_v_v(p2, cy)))


def check_path_valid(p1, p2, obstacle, contour):
    for idx_obstacle in range(0, len(obstacle)):
        current_obstacle = obstacle[idx_obstacle]
        if idx_obstacle == len(obstacle) - 1:
            current_obstacle = contour
        for idx_point in range(0, len(current_obstacle)):
            cur_point = current_obstacle[idx_point]
            pre_point = current_obstacle[(idx_point - 1) % len(current_obstacle)]
            post_point = current_obstacle[(idx_point + 1) % len(current_obstacle)]
            if equal_v_v(p1, cur_point) \
                    and side_check(pre_point, p2, p1, p1) < 0 < side_check(post_point, p2, p1, p1):
                return False
            if equal_v_v(p2, cur_point) \
                    and side_check(pre_point, p1, p2, p2) < 0 < side_check(post_point, p1, p2, p2):
                return False
            if side_check(cur_point, p2, p1, p1) * side_check(post_point, p2, p1, p1) < 0 \
                    and side_check(p1, post_point, cur_point, cur_point) \
                            * side_check(p2, post_point, cur_point, cur_point) < 0:
                return False
            if abs(side_check(p1, post_point, cur_point, cur_point)) < ERR_TOLERANCE \
                    and side_check(cur_point, p2, p1, p1) * side_check(post_point, p2, p1, p1) < 0 \
                    and side_check(post_point, p2, cur_point, cur_point) < 0:
                return False
            if abs(side_check(cur_point, p2, p1, p1)) < ERR_TOLERANCE \
                    and side_check(p1, post_point, cur_point, cur_point) \
                            * side_check(p2, post_point, cur_point, cur_point) < 0:
                return False
    return True


def find_next_expand(point_sets):
    expand_set_idx = len(point_sets) - 1
    expand_point_idx = 1
    min_path_distance = MAX_DISTANCE

    for idx_set in range(0, len(point_sets)):
        point_set = point_sets[idx_set]
        for idx_point in range(0, len(point_set)):
            point = point_set[idx_point]
            if point[5] == 0 and point[4] < min_path_distance:
                expand_set_idx = idx_set
                expand_point_idx = idx_point
                min_path_distance = point[4]
    expand_point = point_sets[expand_set_idx][expand_point_idx]
    expand_point[5] = 1
    return [expand_set_idx, expand_point_idx]


def generate_path(point_sets):
    paths = []

    for corners in point_sets:
        for point in corners:
            point += [-1, -1, MAX_DISTANCE, 0]
    point_sets.append([START_POINT + [-1, -1, 0, 0], GOAL_POINT + [-1, -1, MAX_DISTANCE, 0]])

    while True:
        [expand_set_idx, expand_point_idx] = find_next_expand(point_sets)
        expand_point = point_sets[expand_set_idx][expand_point_idx]

        if equal_v_v(expand_point, GOAL_POINT):
            break

        for idx_set in range(0, len(point_sets)):
            point_set = point_sets[idx_set]
            for point_idx in range(0, len(point_set)):
                point = point_set[point_idx]
                if point[5] == 0:
                    if check_path_valid(expand_point, point, point_sets, WORLD_CONTOUR):
                        paths.append([[point[0], point[1]], expand_point])
                        path_length = distance(expand_point, point)
                        if path_length + expand_point[4] < point[4]:
                            point[2] = expand_set_idx
                            point[3] = expand_point_idx
                            point[4] = path_length + expand_point[4]
    return paths


def generate_minimum_path(point_sets):
    current_point = point_sets[len(point_sets) - 1][1]

    paths = []
    points = []
    while current_point[4] != 0:
        next_point = point_sets[current_point[2]][current_point[3]]
        points.append([current_point[0], current_point[1]])
        paths.append([[current_point[0], current_point[1]], [next_point[0], next_point[1]]])
        current_point = next_point
    points.append([current_point[0], current_point[1]])
    points.reverse()
    paths.reverse()
    return [points, paths]


def store_transit_points(transit_points, file_path):
    f = open(file_path, 'w')
    for point in transit_points:
        f.write(str(point[0]) + ' ' + str(point[1]) + '\n')
    f.close()


def sanitize_point(paths):
    path_set = []
    for path in paths:
        segment = [[path[0][0], path[0][1]], [path[1][0], path[1][1]]]
        path_set.append(segment)
    return path_set


class PlanningGUI:
    def __init__(self, master, obstacles):
        # cache root size info
        self.height = master.winfo_height()
        self.width = master.winfo_width()
        # initialize a canvas
        self.canvas = Canvas(master, width=self.width, height=self.height)
        self.canvas.pack()
        # obstacles and start, end position
        self.obstacles = obstacles
        self.start_goal = [START_POINT, GOAL_POINT]
        # initialize some attributes
        self.min_x = 0
        self.min_y = 0
        self.start_x = 0
        self.start_y = 0
        self.offset = 20
        self.step = 0
        self.init_metrics()
        # init frame with information above
        self.init_canvas()
        # cache all the shape object
        self.shapes = []

        self.grown_obstacles = []
        self.minimum_path = []
        self.transit_point = []

    # draw obstacles and start, goal position
    def init_canvas(self):
        for obt in self.obstacles:
            self.draw_polygon(obt, outline='blue', fill='', width='2')
        self.draw_point(self.start_goal[0], fill='red')
        self.draw_text(self.start_goal[0], text='start')
        self.draw_point(self.start_goal[1], fill='red')
        self.draw_text(self.start_goal[1], text='end')

    # init metrics
    def init_metrics(self):
        # fetch only x
        obt_x_list = [pair[0] for obt in self.obstacles for pair in obt]
        # fetch only y
        obt_y_list = [pair[1] for obt in self.obstacles for pair in obt]
        max_x = max(obt_x_list)
        max_y = max(obt_y_list)
        self.min_x = min(obt_x_list)
        self.min_y = min(obt_y_list)
        # compute step and start x, y
        if max_x - self.min_x > max_y - self.min_y:
            self.step = (self.width - 2 * self.offset) / (max_x - self.min_x)
            self.start_x = self.offset
            self.start_y = (max_x - self.min_x - (max_y - self.min_y)) / 2.0 * self.step + self.offset
        else:
            self.step = (self.height - 2 * self.offset) / (max_y - self.min_y)
            self.start_y = self.offset
            self.start_x = (max_y - self.min_y - (max_x - self.min_x)) / 2.0 * self.step + self.offset

    # according to computed metrics, convert original x, y to canvas x, y
    def convert_pos(self, pos_x, pos_y):
        pos_x -= self.min_x
        pos_y -= self.min_y

        pos_x *= self.step
        pos_y *= self.step

        pos_x += self.start_x
        pos_y += self.start_y

        return pos_x, pos_y

    # add shape into cache
    def add_shape(self, shape):
        self.shapes.append(shape)

    # clear all shapes
    def clear_shapes(self):
        for i in self.shapes:
            self.canvas.delete(i)
        self.shapes = []

    # coord format: [[x, y], [x, y], [x, y], ...]
    def draw_polygon(self, coord, **kw):
        temp = [self.convert_pos(*pair) for pair in coord]
        return self.canvas.create_polygon([pos for pair in temp for pos in pair], **kw)

    # center format: [x, y]
    def draw_point(self, center, **kw):
        temp = self.convert_pos(*center)
        # default radius 5
        radius = 5
        return self.canvas.create_oval(temp[0] - radius, temp[1] - radius,
                                       temp[0] + radius, temp[1] + radius,
                                       **kw)

    # coord format: [[x, y], [x, y], ...]
    def draw_line(self, coord, **kw):
        temp = [self.convert_pos(*pair) for pair in coord]
        return self.canvas.create_line([pos for pair in temp for pos in pair], **kw)

    # coord format: [x, y]
    def draw_text(self, coord, **kw):
        temp = self.convert_pos(*coord)
        return self.canvas.create_text(temp[0], temp[1] - 12, **kw)

    # draw convex hull
    def draw_convex_hull(self):
        self.grown_obstacles = grow_obstacles(self.obstacles, ROBOT_CONTOUR)
        for obstacle in self.grown_obstacles:
            self.add_shape(self.draw_polygon(obstacle, fill='', outline='black'))

    def draw_edges(self):
        all_path = generate_path(self.grown_obstacles)
        sanitize_paths = sanitize_point(all_path)
        for path in sanitize_paths:
            self.add_shape(self.draw_line(path, dash=3))

    def draw_shortest_path(self):
        [self.transit_point, self.minimum_path] = generate_minimum_path(self.grown_obstacles)
        for point in self.transit_point:
            self.add_shape(self.draw_point(point, fill='red', outline='red'))
        for path in self.minimum_path:
            self.add_shape(self.draw_line(path, width=2, fill='red'))


# our animation
def show_process(canvas):
    first = True
    sleep(2)
    while True:
        canvas.draw_convex_hull()
        sleep(SLEEP_TIME)
        canvas.draw_edges()
        sleep(SLEEP_TIME)
        canvas.draw_shortest_path()
        sleep(SLEEP_TIME)
        canvas.clear_shapes()
        sleep(SLEEP_TIME)
        if first:
            store_transit_points(canvas.transit_point, TRANSIT_RESULT_FILE_PATH)
            first = False


# create the top-level frame
root = Tk()
# setting attributes for the frame
root.title('Trajectory Planning')
root.resizable(width=False, height=False)
root.geometry('800x800+200-200')
# force update window size info
root.update_idletasks()
# add the canvas onto the frame
app = PlanningGUI(root, read_obstacles(GOAL_FILE_PATH, OBSTACLE_FILE_PATH))
# start animation
th = threading.Thread(target=show_process, args=(app,))
th.setDaemon(True)
th.start()
# the main loop
root.mainloop()
